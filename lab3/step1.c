#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define REPEATS 100000000

int main (void) {
    volatile int a;
    for (register size_t K = 1; K <= 100; ++K) {
        for (register size_t i = 0; i < REPEATS; ++i) {
            if ((i % K) == 0) {
                a = 1;
            }
        }
    }

    uint64_t start;
    uint64_t end;
    FILE* file = fopen("result_step_1.csv", "w");
    for (register size_t K = 1; K <= 100; ++K) {
        start = __rdtsc();
        for (register size_t i = 0; i < REPEATS; ++i) {
            if ((i % K) == 0)
                a = 1;
        }
        end = __rdtsc();
        printf("K: %3zu, Result: %.4lf\n", K, (double) (end - start) / REPEATS);
        fprintf(file, "%zu, %.4lf\n", K, (double) (end - start) / REPEATS);
    }
    fclose(file);
    return 0;
}