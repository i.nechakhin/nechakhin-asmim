	.file	"step1.c"
	.text
	.section	.rodata
.LC0:
	.string	"w"
.LC1:
	.string	"result_step_1.csv"
.LC3:
	.string	"K: %3zu, Result: %.4lf\n"
.LC4:
	.string	"%zu, %.4lf\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB4262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	$1, %r12d
	jmp	.L2
.L6:
	movl	$0, %ebx
	jmp	.L3
.L5:
	movq	%rbx, %rax
	movl	$0, %edx
	divq	%r12
	movq	%rdx, %rax
	testq	%rax, %rax
	jne	.L4
	movl	$1, -44(%rbp)
.L4:
	addq	$1, %rbx
.L3:
	cmpq	$99999999, %rbx
	jbe	.L5
	addq	$1, %r12
.L2:
	cmpq	$100, %r12
	jbe	.L6
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	fopen@PLT
	movq	%rax, -40(%rbp)
	movl	$1, %ebx
	jmp	.L7
.L17:
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -32(%rbp)
	movl	$0, %r12d
	jmp	.L9
.L11:
	movq	%r12, %rax
	movl	$0, %edx
	divq	%rbx
	movq	%rdx, %rax
	testq	%rax, %rax
	jne	.L10
	movl	$1, -44(%rbp)
.L10:
	addq	$1, %r12
.L9:
	cmpq	$99999999, %r12
	jbe	.L11
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	subq	-32(%rbp), %rax
	testq	%rax, %rax
	js	.L13
	cvtsi2sdq	%rax, %xmm0
	jmp	.L14
.L13:
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
.L14:
	movsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movq	%rbx, %rsi
	leaq	.LC3(%rip), %rdi
	movl	$1, %eax
	call	printf@PLT
	movq	-24(%rbp), %rax
	subq	-32(%rbp), %rax
	testq	%rax, %rax
	js	.L15
	cvtsi2sdq	%rax, %xmm0
	jmp	.L16
.L15:
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
.L16:
	movsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movq	-40(%rbp), %rax
	movq	%rbx, %rdx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movl	$1, %eax
	call	fprintf@PLT
	addq	$1, %rbx
.L7:
	cmpq	$100, %rbx
	jbe	.L17
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	fclose@PLT
	movl	$0, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4262:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC2:
	.long	0
	.long	1100470148
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04.1) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
