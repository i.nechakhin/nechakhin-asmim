#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define REPEATS 100000000

int main(void) {
    volatile int a;
    volatile int b;
    volatile int c = 0;
    for (register size_t i = 0; i < REPEATS; ++i) {
        if ((i % 60) == 0) a = 1;
        else a = 0;
        if ((i % 59) == 0) b = 1;
        else b = 0;
        if ((a * b) == 1) c = 1;
    }

    uint64_t start;
    uint64_t end;
    start = __rdtsc();
    for (register size_t i = 0; i < REPEATS; ++i) {
        if ((i % 60) == 0) a = 1;
        else a = 0;
        if ((i % 59) == 0) b = 1;
        else b = 0;