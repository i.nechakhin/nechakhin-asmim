import matplotlib.pyplot as plt

log = open("my_pc/result_step_6.csv", "r")
lines = log.readlines()

x_axis = []
y_axis = []

for line in lines:
    data = line.split(',')
    if data[0] == 'K':
        continue
    x_axis.append(int(data[0]))
    y_axis.append(float(data[1]))

plt.plot(x_axis, y_axis)
plt.xlabel("Pattern length")
plt.ylabel("Time per iter")
plt.title("Time per iter - Pattern length dependency")

plt.show()