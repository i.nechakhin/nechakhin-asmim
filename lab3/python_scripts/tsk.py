import csv

results = []
with open('cafedral_server/result_main.csv') as file:
    reader = csv.DictReader(file, delimiter=",")
    N = 64
    sum_low = 0
    t = 0
    for row in reader:
        if int(row['K']) <= N:
            sum_low += float(row['Result'].replace(' ', ''))
        else:
            t = float(row['Result'].replace(' ', ''))
            break
    print((t - (sum_low / N)) * (N - 1))
