rm result_step_4.csv

for i in $(seq 1 100)
do
  cat step4_prefix.c > step4.c

  for j in $(seq 1 $i)
  do
    echo "        if (i == REPEATS) a = 1;" >> step4.c
  done

  cat step4_postfix.c >> step4.c

  echo "===== Compiling $i ========"
  gcc -O0 step4.c
  echo "===== Running $i =========="

  RESULT=$(./a.out)
  echo $RESULT
  echo "$i, $RESULT" >> result_step_4.csv

  echo "==========================="
done