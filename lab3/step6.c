#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define REPEATS 100000000

#define FALSE_JUMP \
  if (i == REPEATS)  \
    a = 1;
#define FALSE_JUMP_5 \
  FALSE_JUMP        \
  FALSE_JUMP        \
  FALSE_JUMP        \
  FALSE_JUMP        \
  FALSE_JUMP
#define FALSE_JUMP_10 \
  FALSE_JUMP_5        \
  FALSE_JUMP_5
#define FALSE_JUMP_50 \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10
#define FALSE_JUMP_92 \
  FALSE_JUMP_50       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP_10       \
  FALSE_JUMP          \
  FALSE_JUMP          \


int main (void) {
    volatile int a = 0;
    for (register size_t K = 1; K <= 100; ++K) {
        for (register size_t i = 0; i < REPEATS; ++i) {
            if ((i % K) == 0) {
                a = 1;
            }
        }
    }

    uint64_t start;
    uint64_t end;
    FILE* file = fopen("result_step_6.csv", "a");
    for (register size_t K = 1; K <= 100; ++K) {
        start = __rdtsc();
        for (register size_t i = 0; i < REPEATS; ++i) {
            if ((i % K) == 0)
                a = 0;
            FALSE_JUMP_92
        }
        end = __rdtsc();
        printf("K: %3zu, Result: %.4lf\n", K, (double) (end - start) / REPEATS);
        fprintf(file, "%zu, %.4lf\n", K, (double) (end - start) / REPEATS);
    }
    fclose(file);

    return 0;
}
