#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <x86intrin.h>

#define CYCLE_COUNT 10000000
#define ARRAY_SIZE (10 * 10 * 1024 * 1024 / sizeof(int))

int arr[ARRAY_SIZE];

void swap (int* elem1, int* elem2) {
    int temp = *elem1;
    *elem1 = *elem2;
    *elem2 = temp;
}

void arr_random_init() {
    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        arr[i] = (int) i + 1;
    }
    arr[ARRAY_SIZE - 1] = 0;
    for (size_t i = 0; i < ARRAY_SIZE - 1; ++i) {
        swap(&arr[i], &arr[rand() % ARRAY_SIZE]);
    }
}

int main() {
    arr_random_init();
    uint64_t start;
    uint64_t end;
    int k = 0;

    start = __rdtsc();
    for (size_t i = 0; i < CYCLE_COUNT; ++i) {
        k = arr[k];