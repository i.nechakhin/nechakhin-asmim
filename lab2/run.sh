#!/bin/bash

if [ -z "$1" ]; then
  echo "Arg: <nops-max>"
  exit 1
fi

rm result1.csv

for i in $(seq 1 $1)
do
  cat main_prefix.c > main.c

  for j in $(seq 1 $i)
  do
    echo "        asm(\"nop\");" >> main.c
  done

  cat main_postfix.c >> main.c

  echo "===== Compiling $i ========"
  gcc -O2 main.c
  echo "===== Running $i =========="

  RESULT=$(./a.out)
  echo $RESULT
  echo "$i,$RESULT" >> result1.csv

  echo "==========================="
done