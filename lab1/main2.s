	.file	"main.c"
	.text
	.section	.rodata
	.align 8
.LC0:
	.string	"Processor warmed up. result = %d\n"
	.align 8
.LC1:
	.string	"Time per empty iteration = %lf\n"
	.align 8
.LC2:
	.string	"Time for independent operations = %lf, res = %d\n"
	.align 8
.LC3:
	.string	"Time for dependent operations = %lf, res = %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	$-2147483648, %r12d
	movl	$2, %r13d
	movl	$0, %r14d
	movl	$0, %edi
	call	time@PLT
	movq	%rax, -80(%rbp)
	jmp	.L2
.L3:
	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d
.L2:
	movl	$0, %edi
	call	time@PLT
	subq	-80(%rbp), %rax
	cmpq	$59, %rax
	jle	.L3
	movl	%r14d, %esi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
#APP
# 25 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)

    movl	$0, %ebx
	jmp	.L4
.L5:
	addl	$24, %ebx
.L4:
	cmpl	$4166665, %ebx
	jle	.L5

#APP
# 28 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	testq	%rax, %rax
	js	.L6
	cvtsi2sdq	%rax, %xmm0
	jmp	.L7
.L6:
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
.L7:
	cvtsi2sdl	%ebx, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %xmm0
	leaq	.LC1(%rip), %rdi
	movl	$1, %eax
	call	printf@PLT
#APP
# 34 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)

    movl	$0, %ebx
	jmp	.L8
.L9:
	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
	movl	$0, %edx
	divl	%r13d
	movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	movl	%r12d, %eax
    movl	$0, %edx
    divl	%r13d
    movl	%eax, %r14d

	addl    $24, %ebx
.L8:
	cmpl	$4166665, %ebx
	jle	.L9

#APP
# 38 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	testq	%rax, %rax
	js	.L10
	cvtsi2sdq	%rax, %xmm0
	jmp	.L11
.L10:
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
.L11:
	cvtsi2sdl	%ebx, %xmm1
	divsd	%xmm1, %xmm0
	subsd	-72(%rbp), %xmm0
	movl	%r14d, %esi
	leaq	.LC2(%rip), %rdi
	movl	$1, %eax
	call	printf@PLT
	movl	$-2147483648, %r12d
#APP
# 45 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)

	movl	$0, %ebx
	jmp	.L12
.L13:
	movl	%r12d, %eax

	movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
	divl	%r13d

	movl	$0, %edx
    divl	%r13d

	movl	$0, %edx
    divl	%r13d

    movl	$0, %edx
    divl	%r13d

    movl    %eax, %r12d

	addl	$24, %ebx
.L12:
	cmpl	$4166665, %ebx
	jle	.L13

#APP
# 49 "main.c" 1
	rdtsc

# 0 "" 2
#NO_APP
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	testq	%rax, %rax
	js	.L14
	cvtsi2sdq	%rax, %xmm0
	jmp	.L15
.L14:
	movq	%rax, %rdx
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
.L15:
	cvtsi2sdl	%ebx, %xmm1
	divsd	%xmm1, %xmm0
	subsd	-72(%rbp), %xmm0
	movl	%r12d, %esi
	leaq	.LC3(%rip), %rdi
	movl	$1, %eax
	call	printf@PLT
	movl	$0, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04.1) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
