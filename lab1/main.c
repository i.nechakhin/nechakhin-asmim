#include <stdio.h>
#include <time.h>

#define WARMING_UP_SECONDS 60
#define CYCLE_COUNT 100000000

int main () {

    register unsigned int x = 1 << 31;
    register unsigned int y = 2;
    register unsigned int res = 0;

    time_t start_time = time(NULL);
    while (time(NULL) - start_time < WARMING_UP_SECONDS) {
        res = x / y;
    }
    printf("Processor warmed up. result = %d\n", res);

    union ticks {
        unsigned long long t64;
        struct s32 { long th, tl; } t32;
    } start, end;

    register int i;
    asm("rdtsc\n":"=a"(start.t32.th), "=d"(start.t32.tl));
    for (i = 0; i < CYCLE_COUNT; ++i) {
    }
    asm("rdtsc\n":"=a"(end.t32.th), "=d"(end.t32.tl));
    double time_per_empty_iter = (double) (end.t64 - start.t64) / i;
    printf("Time per empty iteration = %lf\n", time_per_empty_iter);

    //Independent operations

    asm("rdtsc\n":"=a"(start.t32.th), "=d"(start.t32.tl));
    for (i = 0; i < CYCLE_COUNT; ++i) {
        res = x / y;
    }
    asm("rdtsc\n":"=a"(end.t32.th), "=d"(end.t32.tl));
    printf("Time for independent operations = %lf, res = %d\n", (double) (end.t64 - start.t64) / i - time_per_empty_iter, res);

    //Dependent operations

    x = 1 << 31;

    asm("rdtsc\n":"=a"(start.t32.th), "=d"(start.t32.tl));
    for (i = 0; i < CYCLE_COUNT; ++i) {
        x = x / y;
    }
    asm("rdtsc\n":"=a"(end.t32.th), "=d"(end.t32.tl));
    printf("Time for dependent operations = %lf, res = %d\n", (double) (end.t64 - start.t64) / i - time_per_empty_iter, x);

    return 0;
}
